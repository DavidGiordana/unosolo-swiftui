//
//  Extensions.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/24/21.
//

import Foundation

extension Array where Element: Equatable {
    
    func index(equals element: Element) -> Self.Index? {
        return firstIndex(where: { element == $0 })
    }
    
}

extension Collection {
    
    func any(check: (Element) -> Bool) -> Bool {
        for element in self {
            if check(element) {
                return true
            }
        }
        
        return false
    }
    
    var isNotEmpty: Bool {
        return !isEmpty
    }
    
}
