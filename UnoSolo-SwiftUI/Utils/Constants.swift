//
//  Constants.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/24/21.
//

import SwiftUI

struct Constants {
    static let boxPadding: CGFloat = 5
    static let boxColor = Color.black.opacity(0.2)
    
    static let piecePadding: CGFloat = 14
    static let selectediecePadding: CGFloat = 5
    
    static let squareInternalCirclePadding: CGFloat = 14
    static func squareExternalCirclePadding(isCenter: Bool) -> CGFloat {
        return isCenter ? 6 : 10
    }
}
