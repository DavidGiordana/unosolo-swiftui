//
//  UnoSolo_SwiftUIApp.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/23/21.
//

import SwiftUI

@main
struct UnoSolo_SwiftUIApp: App {

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
