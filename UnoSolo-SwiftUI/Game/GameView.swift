//
//  GameView.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/24/21.
//

import SwiftUI

struct GameView: View {
    
    @EnvironmentObject var game: Game
    
    var body: some View {
        GeometryReader { geometry in
            let side = min(geometry.size.width, geometry.size.height)
            let tileSize = side / CGFloat(game.board.sideSquaresCount)
            
            ZStack(alignment: .topLeading) {
                RoundedRectangle(cornerRadius: 7)
                    .fill(Color.orange)
                
                boxes(withSquareSize: tileSize)
                squares(tileSize: tileSize)
                
                if game.canPlay {
                    pieces(tileSize: tileSize)
                    possiblePlays(tileSize: tileSize)
                } else {
                    gameOverView(size: side)
                }
            }
            .frame(width: side, height: side)
            .position(
                x: geometry.size.width / 2,
                y: geometry.size.height / 2
            )
        }
    }
    
    // MARK: - Components
    
    private func squares(tileSize: CGFloat) -> some View {
        ForEach(game.squares) { position in
            BoardSquareView(
                position: position,
                tileSize: tileSize,
                isCenter: game.isCenter(position: position)
            )
        }
    }
    
    private func pieces(tileSize: CGFloat) -> some View {
        ForEach(game.pieces) { position in
            PieceView(
                position: position,
                tileSize: tileSize,
                selected: game.selectedPosition == position
            )
            .onTapGesture {
                withAnimation(.linear(duration: 0.1)) {
                    game.selectedPosition = position
                }
            }
        }
    }
    
    private func possiblePlays(tileSize: CGFloat) -> some View {
        ForEach(game.possiblePlays) { possiblePlay in
            PieceSelectionView(position: possiblePlay.finalPosition, tileSize: tileSize)
                .onTapGesture {
                    withAnimation {
                        game.play(to: possiblePlay)
                    }
                }
        }
    }
    
    func gameOverView(size: CGFloat) -> some View {
        ZStack {
            Color.white
                .opacity(0.25)
            Text("GAME OVER")
                .font(Font.bold(.largeTitle)())
                .padding(20)
                .foregroundColor(.white)
                .background(
                    RoundedRectangle(cornerRadius: 25.0)
                        .stroke(Color.black.opacity(0.25), lineWidth: 3)
                        .background(Color.orange)
                )
                
        }
        .position(x: size / 2, y: size / 2)
        .animation(.easeIn)
    }
    
    // MARK: Boxes
    
    private func boxes(withSquareSize squareSize: CGFloat) -> some View {
        let boxSideSize = squareSize * CGFloat(game.board.cornerBoxSquaresCount) - Constants.boxPadding * 2
        let oppostieSideStartIndex = CGFloat(game.board.sideSquaresCount - game.board.cornerBoxSquaresCount)
        let oppostieSideStartOffset = squareSize * oppostieSideStartIndex
        
        return Group {
            box(
               boxSideSize: boxSideSize,
               offsetX: 0,
               offsetY: 0
           )
            box(
               boxSideSize: boxSideSize,
               offsetX: 0,
               offsetY: oppostieSideStartOffset
           )
            box(
               boxSideSize: boxSideSize,
               offsetX: oppostieSideStartOffset,
               offsetY: 0
           )
            box(
               boxSideSize: boxSideSize,
               offsetX: oppostieSideStartOffset,
               offsetY: oppostieSideStartOffset
           )
        }
    }
    
    private func box(boxSideSize: CGFloat, offsetX: CGFloat, offsetY: CGFloat) -> some View {
        RoundedRectangle(cornerRadius: 7)
            .fill(Constants.boxColor)
            .frame(width: boxSideSize, height: boxSideSize)
            .offset(x: offsetX, y: offsetY)
            .padding(Constants.boxPadding)
    }
    
}

fileprivate struct BoardSquareView: View {
    
    let position: Board.Position
    let tileSize: CGFloat
    let isCenter: Bool
    
    var body: some View {
        ZStack {
            Circle()
                .fill(Color.black.opacity(0.3))
                .padding(Constants.squareExternalCirclePadding(isCenter: isCenter))
            
            Circle()
                .fill(Color.orange.opacity(0.4))
                .padding(Constants.squareInternalCirclePadding)
        }
        .offset(
            x: CGFloat(position.col) * tileSize,
            y: CGFloat(position.row) * tileSize
        )
        .frame(width: tileSize, height: tileSize)
    }
    
}

fileprivate struct PieceView: View {
    
    let position: Board.Position
    let tileSize: CGFloat
    let selected: Bool
    
    var body: some View {
        ZStack {
            if selected {
                PieceSelectionView(position: position, tileSize: tileSize)
            }
                
            Circle()
                .fill(Color.yellow)
                .padding(Constants.piecePadding)
                .offset(
                    x: CGFloat(position.col) * tileSize,
                    y: CGFloat(position.row) * tileSize
                )
                .frame(width: tileSize, height: tileSize)
        }
    }
    
}

fileprivate struct PieceSelectionView: View {
    
    let position: Board.Position
    let tileSize: CGFloat
    
    var body: some View {
        Circle()
            .fill(Color.blue.opacity(0.7))
            .padding(Constants.selectediecePadding)
            .offset(
                x: CGFloat(position.col) * tileSize,
                y: CGFloat(position.row) * tileSize
            )
            .frame(width: tileSize, height: tileSize)
            .blur(radius: 5)
    }
    
}

struct BoardView_Previews: PreviewProvider {
    static var previews: some View {
        GameView()
            .previewDevice(.init(stringLiteral: "iPhone 11"))
    }
}
