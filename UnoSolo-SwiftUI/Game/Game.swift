//
//  Game.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/24/21.
//

import Foundation

class Game: ObservableObject {
    
    // MARK: - Types
    
    typealias Position = Board.Position
    
    struct PossiblePlay: Identifiable {
        let id: String
        let middlePosition: Position
        let finalPosition: Position
    }
    
    // MARK: - Data
    
    @Published var board = Board()
    
    @Published var selectedPosition: Position? {
        didSet {
            if let position = selectedPosition {
                possiblePlays = calculatePossiblePlays(selectedPosition: position)
            } else {
                possiblePlays = []
            }
        }
    }
    
    var squares: [Position] { board.squares }
    
    var pieces: [Position] { board.pieces }
    
    var possiblePlays = [PossiblePlay]()
    
    var canPlay: Bool {
        pieces.any { calculatePossiblePlays(selectedPosition: $0).isNotEmpty }
    }
    
    func isCenter(position: Board.Position) -> Bool {
        return board.isCenter(position: position)
    }
    
    func play(to possiblePlay: PossiblePlay) {
        guard
            let selectedPosition = self.selectedPosition,
            let selectedIndex = pieces.index(equals: selectedPosition),
            let middleIndex = pieces.index(equals: possiblePlay.middlePosition)
        else { return }
                
        board.pieces[selectedIndex].copyPosition(from: possiblePlay.finalPosition)
        board.pieces.remove(at: middleIndex)
        
        self.selectedPosition = nil
    }
    
    func reset() {
        
    }
    
    // MARK: - Internal
    
    private func calculatePossiblePlays(selectedPosition: Position) -> [PossiblePlay] {
        return [
            PossiblePlay(
                id: "top",
                middlePosition: Position(row: selectedPosition.row - 1, col: selectedPosition.col),
                finalPosition: Position(row: selectedPosition.row - 2, col: selectedPosition.col)
            ),
            PossiblePlay(
                id: "bottom",
                middlePosition: Position(row: selectedPosition.row + 1, col: selectedPosition.col),
                finalPosition: Position(row: selectedPosition.row + 2, col: selectedPosition.col)
            ),
            PossiblePlay(
                id: "left",
                middlePosition: Position(row: selectedPosition.row, col: selectedPosition.col - 1),
                finalPosition: Position(row: selectedPosition.row, col: selectedPosition.col - 2)
            ),
            PossiblePlay(
                id: "right",
                middlePosition: Position(row: selectedPosition.row, col: selectedPosition.col + 1),
                finalPosition: Position(row: selectedPosition.row, col: selectedPosition.col + 2)
            )
        ]
        .filter(canPlay)
    }
    
    private func canPlay(_ possiblePlay: PossiblePlay) -> Bool {
        return
            pieces.contains(possiblePlay.middlePosition) &&
            squares.contains(possiblePlay.finalPosition) &&
            !pieces.contains(possiblePlay.finalPosition)
    }
    
}
