//
//  Board.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/24/21.
//

import Foundation

struct Board {
    
    struct Position: Identifiable, Equatable {
        
        let id: String
        
        var row: Int
        var col: Int
        
        init(row: Int, col: Int) {
            self.row = row
            self.col = col
            self.id = "\(row)-\(col)"
        }
        
        mutating func copyPosition(from newPosition: Position) {
            self.row = newPosition.row
            self.col = newPosition.col
        }
        
        static func ==(lhs: Self, rhs: Self) -> Bool {
            return lhs.row == rhs.row && lhs.col == rhs.col
        }
        
    }
        
    let sideSquaresCount = 7
    let cornerBoxSquaresCount = 2
    
    let centerPosition: Int
    
    private(set) var squares = [Position]()
        
    var pieces = [Position]()
    
    init() {
        centerPosition = sideSquaresCount / 2
        for row in 0..<sideSquaresCount {
            for col in 0..<sideSquaresCount {
                if !(
                    (row < cornerBoxSquaresCount && col < cornerBoxSquaresCount) ||
                    (row >= (sideSquaresCount - cornerBoxSquaresCount) && col < cornerBoxSquaresCount) ||
                    (row < cornerBoxSquaresCount && col >= (sideSquaresCount - cornerBoxSquaresCount)) ||
                    (row >= (sideSquaresCount - cornerBoxSquaresCount) && col >= (sideSquaresCount - cornerBoxSquaresCount))
                ) {
                    self.squares.append(Position(row: row, col: col))
                }
            }
        }
        
        pieces = squares.filter { !isCenter(position: $0) }
    }
    
    func isCenter(position: Board.Position) -> Bool {
        return position.row == position.col && position.row == centerPosition
    }
    
}
