//
//  ContentView.swift
//  UnoSolo-SwiftUI
//
//  Created by David Giordana on 8/26/21.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var game = Game()
    
    var body: some View {
        NavigationView {
            VStack(alignment: .center) {
                GameView()
                    .environmentObject(game)
                resetButton
            }
            .navigationTitle("Uno Solo")
            .padding()
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    var resetButton: some View {
        Button {
            game.reset()
        } label: {
            Text("Reset")
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice(.init(stringLiteral: "iPhone 11"))
    }
}
